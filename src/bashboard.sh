#!/bin/bash

# Directory of the script, not the working directory
DIR=$( cd -- "$( dirname -- "$(readlink ${BASH_SOURCE[0]})" )" &> /dev/null && pwd )

# Configs
source "${DIR}/colors.source"
source "${DIR}/modules/config/privacy.cfg"

# Exit if privacy file is true
[ $PRIVACY = true ] && echo "BashBoard: Privacy is set to true" && exit

paste <(echo -e "    =====================================================\n\
$(figlet -f slant "BashBoard")\
\n===================================================        " | lolcat -f --random && echo -en "                \e[39mby Darwin Brandao\n\n") <([ $(tput cols) -gt 87 ] && echo -en "\n\e[39m$(source "${DIR}/modules/weather.source" | awk '{print "\033[39m"$0}' | ([ $PRIVACY = true ] && sed "s/$( source "${DIR}/modules/config/wttr.cfg"; echo ${city} )/#####/" || awk '{print}'))")

# Load modules
while read -r line
do
	source "${DIR}/$line"
done < "${DIR}/modules.cfg"

# Print results as columns
paste <(printf "$todos") <(printf "$agenda") <(printf "$wishes") -d "|" | column -t -s "|"

echo

# TODO: blink appointment 15 min before it
