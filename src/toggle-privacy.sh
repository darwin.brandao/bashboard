#!/bin/bash

# Directory of the script, not the working directory
DIR=$( cd -- "$( dirname -- "$(readlink ${BASH_SOURCE[0]})" )" &> /dev/null && pwd )

source "${DIR}/modules/config/privacy.cfg"

feedback="Changing from ${PRIVACY} to "

if [ $PRIVACY = false ]; then
	PRIVACY=true
else
	PRIVACY=false
fi

feedback="${feedback}${PRIVACY}"

echo $feedback

echo "PRIVACY=${PRIVACY}" > "${DIR}/modules/config/privacy.cfg"
