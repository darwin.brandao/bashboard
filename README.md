# Bashboard

![Screen Shot](screenshot.png)

Bashboard is a script I wrote to have all the important information I need in one place.

I added to my "~/.bashrc", so I can see my TO-DO list, my Agenda and other information everytime I open a terminal. I recommend you do the same.

It uses 'calcurse' to manage to-do list and agenda. If you don't know calcurse, take a look at [calcurse's website](https://calcurse.org/)

## Dependencies

### Lolcat

[Lolcat GitHub repo](https://github.com/busyloop/lolcat)

Lolcat is probably available in your package manager's repo.

Debian based systems:
```
sudo apt install lolcat
```

Fedora based systems:
```
sudo dnf install lolcat
```

Arch based systems:
```
sudo pacman -S lolcat
```

### FIGlet

[FIGlet GitHub repo](https://github.com/cmatsuoka/figlet)

Figlet is probably available in your package manager's repo.

Debian based systems:
```
sudo apt install figlet
```

Fedora based systems:
```
sudo dnf install figlet
```

Arch based systems:
```
sudo pacman -S figlet
```

### cURL

[cURL GitHub repo](https://github.com/curl/curl)

cURL is probably available in your package manager's repo.

Debian based systems:
```
sudo apt install curl
```

Fedora based systems:
```
sudo dnf install curl
```

Arch based systems:
```
sudo pacman -S curl
```

### Calcurse

As mentioned above, 'calcurse' is very important to make it work as expected. You can modify the script to read other calendar app entries, but I use 'calcurse' and I wrote this script to work with it.

Calcurse is probably available in your package manager's repo.

Debian based systems:
```
sudo apt install calcurse
```

Fedora based systems:
```
sudo dnf install calcurse
```

Arch based systems:
```
sudo pacman -S calcurse
```

### WishList

This is a script I wrote to keep track of things I want to buy or do that are not important enough to be in a TO-DO list.

You can find it here: [wishlist's repo](https://gitlab.com/darwin.brandao/wishlist)

The 'WishList' column in Bashboard shuffles the wishes and shows the top 5 results.

## License
MIT License
